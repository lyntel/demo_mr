#include <iostream>
#include "test.h"

int main() {
    Test* instance = new Test();
    if(instance != nullptr) {
        int result = instance->getValueA();
        std::cout << "result:" << result << std::endl;
    }
    return 0;
}